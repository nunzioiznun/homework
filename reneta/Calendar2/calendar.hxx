//
// Created by ebcrens on 2018-05-04.
//
#pragma once
#include "utils.hxx"
#include <iostream>
#include <array>
#include <algorithm>
#include <iomanip>
#include <map>
#include <chrono>
#include <regex>

using namespace std;
const string SE = "se";
const string EN = "en";

class Calendar {
private:
    string lang;//default EN
    unsigned short year; //default = getCurrentYear();
    unsigned short month; //default =  getCurrentMonth();
    string startDay; //default = "Mon";
    map<string, unsigned short> months;
public:
    bool isMonthInterval = false;
    Calendar (const string &lang = EN, unsigned short year = getCurrentYear(), unsigned short month = getCurrentMonth(), const string &startDay = "Mon") :
            lang(lang), year(year), month(month), startDay(startDay) {
        populateMonthMap();
    }

    friend ostream &operator<< (ostream &os, const Calendar &calendar) {
        os << "lang: " << calendar.lang << " year: " << calendar.year << " month: " << calendar.month << " startDay: "
           << calendar.startDay;
        return os;
    }

    const string &getLang () const {
        return lang;
    }

    void setLang (const string &lang) {
        Calendar::lang = lang;
    }

    unsigned short getYear () const {
        return year;
    }

    void setYear (unsigned short year) {
        Calendar::year = year;
    }

    unsigned short getMonth () const {
        return month;
    }

    void setMonth (unsigned short month) {
        Calendar::month = month;
    }

    const string &getStartDay () const {
        return startDay;
    }

    void setStartDay (const string &startDay) {
        Calendar::startDay = startDay;
    }

    void populateMonthMap(){
        if (lang == SE) {
            months.emplace("januari", 1);
            months.emplace("februari", 2);
            months.emplace("mars", 3);
            months.emplace("april", 4);
            months.emplace("maj", 5);
            months.emplace("juni", 6);
            months.emplace("juli", 7);
            months.emplace("augusti", 8);
            months.emplace("september", 9);
            months.emplace("oktober", 10);
            months.emplace("november", 11);
            months.emplace("december", 12);
        } else {
            months.emplace("january", 1);
            months.emplace("february", 2);
            months.emplace("march", 3);
            months.emplace("april", 4);
            months.emplace("may", 5);
            months.emplace("june", 6);
            months.emplace("july", 7);
            months.emplace("august", 8);
            months.emplace("september", 9);
            months.emplace("october", 10);
            months.emplace("november", 11);
            months.emplace("december", 12);
        }
    }
    void validateLanguage () {
        if (is_number(lang)) {
            throw invalid_argument(R"(Language can be "se" or "en" not )"s + lang);
        }
        if (lang != SE && lang != EN) {
            throw invalid_argument(R"(Language supported are "se" or "en" not )"s + lang);
        }
    }

    void validateMonth () {
        if (month < 1 || month > 12) {
            throw out_of_range("Month should be between 1 and 12 not  "s + to_string(month));
        }
    }

    void validateYear () {
        if (year <= 1752 || year >= 9999) {
            throw out_of_range("Year should be between 1752 and 9999 not "s + to_string(year));
        }
    }

    void validateStartDay () {
        if (is_number(startDay)) {
            throw invalid_argument("Start day should be Mon or Sun not "s + startDay);
        }
        if (startDay != "Mon" && startDay != "Sun") {
            throw invalid_argument("Start day should be Mon or Sun not "s + startDay);
        }
    }

    void convertYearStringToYear (const string &yearString) {
        if (is_number(yearString)) {
            year = static_cast<unsigned short>(stoi(yearString));
        } else {
            throw invalid_argument("Year should be a number not "s + yearString);
        }
    }

    void convertMonthStringToMonth (const string &monthString) {
        if (is_number(monthString)) {
            month = static_cast<unsigned short>(stoi(monthString));
            validateMonth();
        } else if (is_alpha(monthString)){
            convertMonthNameToNumber(monthString);
        } else{
            isMonthInterval = true;
        }
    }

    void convertMonthNameToNumber (string monthString) {
        transform(monthString.begin(), monthString.end(), monthString.begin(), ::tolower);
        auto found = months.find(monthString);
        if(found == months.end())
        {
            throw invalid_argument("Invalid argument "s + monthString);
        }
        else{
            month = months.find(monthString)->second;
        }
    }
    string convertMonthToMonthName (unsigned short month) {
        auto monthName = find_if(months.begin(), months.end(), [&] (pair<string, unsigned short> p) {
            return p.second == month;
        })->first;
        monthName[0] = static_cast<char>(toupper(monthName[0]));
        return monthName;
    }

    void printMonthName (unsigned short month, unsigned short year) {
        auto monthName = convertMonthToMonthName(month);
        cout << monthName << " " << year << endl;
    }
    void printHeader (const string &lang, const string &startDay) {
        if (lang == EN) {
            if (startDay == "Sun") {
                cout << setw(5) << "Sun" << "Mon" << setw(5) << "Tues" << setw(5) << "Wed" << setw(5) << "Thu" << setw(5)
                     << "Fri"
                     << setw(5) << "Sat" << setw(5) << endl;
            } else {
                cout << setw(5) << "Mon" << setw(5) << "Tues" << setw(5) << "Wed" << setw(5) << "Thu" << setw(5) << "Fri"
                     << setw(5) << "Sat" << setw(5) << "Sun" << endl;

            }
        } else {
            if (startDay == "Sun") {
                cout << setw(8) << "Sö" << "Må" << setw(5) << "Ti" << setw(6) << "On" << setw(6) << "To" << setw(6) << "Fr"
                     << setw(7) << "Lö" << setw(7) << endl;
            } else {
                wcout << setw(8) << "Må" << setw(5) << "Ti" << setw(6) << "On" << setw(6) << "To" << setw(6) << "Fr"
                      << setw(7) << "Lö" << setw(7) << "Sö" << endl;
            }
        }
    }
    //Count  the weekday of a date
//f = k + [(13*m-1)/5] + D + [D/4] + [C/4] - 2*C. - Zeller's Rule
// f - 7*[f/7] will give you the day of the week.
//Sunday = 0, Monday = 1, and so on.
//k - day of the month.
//m  - month, counting March as 1 and February as 12.
//D  - the last two digits of the year.
//C  - the first two digits of the year.
    unsigned short weekDayNumber (unsigned short day, unsigned short month, unsigned short year) {
        unsigned short k = day;
        auto D = static_cast<unsigned short>(year % 100);
        unsigned short result = 0;
        unsigned short m = 0;
        if (month == 1) {
            m = 11;
            D = static_cast<unsigned short>(D - 1);
        }
        if (month == 2) {
            m = 12;
            D = static_cast<unsigned short>(D - 1);
        }
        if (month > 2 && month <= 12) {
            m = static_cast<unsigned short>(month - 2);
        }
        auto C = static_cast<unsigned short>(year / 100);
        int f = k + (13 * m - 1) / 5 + D + D / 4 + C / 4 - 2 * C;
        if (f >= 0) {
            result = static_cast<unsigned short>(f - 7 * (f / 7));
        } else {
            result = static_cast<unsigned short>(f - 7 * (f / 7) + 7);
        }
        return result;
    }

    unsigned short findWeekDay (unsigned short year, unsigned short month, const string &startDay) {
        unsigned short weekDay = weekDayNumber(1, month, year);

        if (startDay == "Mon") {
            if (weekDay == 0) {
                weekDay = 7;
            }
        }
        return weekDay;
    }
    bool IsLeapYear(const int year) {
        return ((year%400==0) || (year%4==0 && year%100!=0));
    }
    unsigned short numberOfDaysMonth (unsigned short monthNumber, unsigned short year) {
        switch (monthNumber) {
            case 2:
                return IsLeapYear(year) ? 29 : 28;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 31;
        }
    }
    void printDays (unsigned short weekDay, unsigned short daysInMonth, const string &lang, const string &startDay) {
        unsigned short k = 0;
        for (k = 1; k < weekDay; k++) {
            if (lang == SE)
                cout << setw(6) << " ";
            else
                cout << setw(5) << " ";
        }
        for (unsigned short j = 1; j <= daysInMonth; j++) {
            if (lang == SE)
                cout << setw(6) << j;
            else
                cout << setw(5) << j;
            if (startDay == "Mon") {
                if (++k > 7) {
                    k = 1;
                    cout << endl;
                }
            } else {
                if (++k > 6) {
                    k = 0;
                    cout << endl;
                }
            }
        }
        if (k)
            cout <<
                 endl;
    }
    void printMonth (unsigned short year, unsigned short month, const string &lang, const string &startDay) {
        printMonthName(month, year);
        printHeader(lang, startDay);
        unsigned short weekDay = findWeekDay(year, month, startDay);
        unsigned short daysInMonth = numberOfDaysMonth(month, year);
        printDays(weekDay, daysInMonth, lang, startDay);
    }
};



