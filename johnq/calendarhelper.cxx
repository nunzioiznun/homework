#include "calendarhelper.hxx"

//using namespace std;

int readMonth(string& _arg);

bool par_reader(int argc, char **pString, ArgsCalendar& A) {
	if (argc == 1)
		return 0;
	string month_arg;

	// todo: for now one argument to process, read more arguments
	for (int i = 1; i < argc ; ++i) {
		// First argument has to be a Month, if only argument prints the calendar for that
		// current year.
		// In case of a range to print this will be the initial month and next argument final month.
		if (i == 1) {
			month_arg = pString[i];
			try {
				A.monthInitial = readMonth(month_arg);
			}
			catch (const char* msg){
				cout << msg << endl;
				return false;
			}
		}
		// The second argument can be a month or a year.
		// TODO: implement range months
		if (i == 2){
			try {
				int year = stoi(pString[i]);
				A.year = year - 1900;
			}
			catch (invalid_argument& e){
				cout << "Second argument is not valid, should be a month or a year." << endl;
				return false;
			}
		}
	}
	if ( argc == 2 ) A.cmd = "curmonth";
	if ( argc == 3 ) {
		if (A.year != 0) A.cmd = "monthyear";
		else A.cmd = "rangemonths";
	}
	return true;
}

int readMonth(string& _arg){
	int mon;
	
	transform(_arg.begin(), _arg.end(), _arg.begin(),
	          [](unsigned char c) { return tolower(c); });
	if (_arg == "january")
		mon = 0;
	else if (_arg == "february")
		mon = 1;
	else if (_arg == "march")
		mon = 2;
	else if (_arg == "april")
		mon = 3;
	else if (_arg == "may")
		mon = 4;
	else if (_arg == "june")
		mon = 5;
	else if (_arg == "july")
		mon = 6;
	else if (_arg == "august")
		mon = 7;
	else if (_arg == "september")
		mon = 8;
	else if (_arg == "october")
		mon = 9;
	else if (_arg == "november")
		mon = 10;
	else if (_arg == "december")
		mon = 11;
	else {
		throw "Invalid month, use month's full name, non case-sensitive";
	}
	return mon;
}

// TODO: the program seems not to reach the default, maybe is needed to add error handling either case.
// Number of day for a given month, if February checks if it is a leap year.
int numdaysmonth(int _month, bool is_leap) {
	int mo;
	switch (_month) {
		case 0:
		case 2:
		case 4:
		case 6:
		case 7:
		case 9:
		case 11:
			mo = 31;
			break;
		case 3:
		case 5:
		case 8:
		case 10:
			mo = 30;
			break;
		case 1:
			mo = is_leap ? 29 : 28;
			break;
		default:
			cout << "Unrecognized month" << '\n';
	}
	return mo;
}

// Implement leap function
bool is_leap(int year) {
	return year % 4 == 0 && (year % 100 == 0 && year % 400 == 0) ? true : false;
}

// TODO: Add logic to change first day of the week to Monday.
// prints any month, Sunday is 0.
void printCalendar(tm* pTm, int daysMonth) {
	cout << "   " << put_time(pTm, "%B %Y") << "\n\n";
	cout << setw(6)
	     << "Sun" << setw(5) << "Mon" << setw(5) << "Tue" << setw(5) << "Wed" << setw(5)
	     << setw(5) << "Thu" << setw(5) << "Fri" << setw(5) << "Sat" << "\n\n";
	for (int j = 1; j <= daysMonth;) {
		for (int i = 0; i < 7 ; ++i) {
			if (i < (*pTm).tm_wday && j == 1) {
				cout << setw(5) << ' ';
				continue;
			}
			cout << setw(5) << j;
			if (j++ >= daysMonth)
				break;
		}
		cout << "\n\n";
	}
}

void monthFirstDay(tm* t){
	// Find first day of the month.
	(*t).tm_mday = 1;
	mktime(t);
}


