#include <iostream>
#include <ctime>
#include <iomanip>
#include <vector>

using namespace std;

static const vector<string> sMonth{{"January","February","Mars","April","May","June","July","August","September", "October","November","December"}};


int getMaxDaysForMonth(int year, const int month) {
    //Month should be tm_mon which goes from [0-11], where 0=January etc.
    year += 1900; //tm_year is years since 1900
    switch (month) {
        case 0: case 2: case 4: case 6: case 7: case 9: case 11:
            return 31;
        case 3: case 5: case 8: case 10:
            return 30;
        case 1: {if ((year%400==0)||(year%4==0)&&(year%100!=0)) return 29; else return 28;}
        default: return -1;
    }

}

string createWeek(int &day, const int maxDays, const int weekStartPos) {
    string s=""s;
    for (int pos=1;pos<8;pos++) {
        if ((pos<weekStartPos) || (day>maxDays)) {
            s+="    ";
        } else {
            if (day<=9) {
                s+=" ";
            }
            s+=to_string(day)+"  ";
            day++;
        }
    }
    return s;
}

int getWeekStartpos(const int weekday) {
    // int tm_wday: Days since Sunday 0-6, i.e. Sunday=0, Monday=1,...Saturday=6
    //We want Monday=1, Tue=2, .... Sunday=7
    if (weekday==0) return 7; else return weekday;
}

tm* getCurrentDate() {
    time_t time1 = time(nullptr);
    //cout << time1 << endl;
    //cout << ctime(&time1);
    tm *timePtr = localtime(&time1);
    //cout << put_time(timePtr, "%Y %b %d") << endl;
    return timePtr;
}

int getMonth(int nArgs, char* args[], int month) {
    if (nArgs>=2) {
        //expecting month
        string usrInput = args[1];
        int enteredMonth=0;
        for (;enteredMonth<sMonth.size();enteredMonth++) {
            if (usrInput==sMonth[enteredMonth]) break;
        }
        if ((enteredMonth>=0)&&(enteredMonth<=11)) {
            month = enteredMonth;
        } else {
            cout << usrInput << " is not a valid month. Instead using current month "s << endl;
        }
    }
    return month;
}

int getYear(int nArgs, char* args[], int year) {
    if (nArgs>=3) {
        //expecting year
        string usrInput = args[2];
        year = stoi(usrInput)-1900;//tm_year is years since 1900
    }
    return year;
}

void printHeading(int year, int month) {
    cout << sMonth[month] << " " << to_string(year+1900) << endl; //tm_year is years since 1900
    cout << "Mon Tue Wed Thu Fri Sat Sun" << endl;
}

int main(int nArgs, char* args[]) {
    tm *timePtr = getCurrentDate();
    timePtr->tm_year = getYear(nArgs, args, timePtr->tm_year);
    timePtr->tm_mon = getMonth(nArgs, args, timePtr->tm_mon);
    printHeading(timePtr->tm_year, timePtr->tm_mon);
    int maxDays = getMaxDaysForMonth(timePtr->tm_year,timePtr->tm_mon);
    timePtr->tm_mday = 1;  //calendar should start at first day of month
    int day=timePtr->tm_mday;
    mktime(timePtr);  //needed for calculate timePtr->tm_wday (weekday)
    int weekStartPos=getWeekStartpos(timePtr->tm_wday); //first week on Month can start on other day than Monday
    while (day<=maxDays) {
        string sWeek = createWeek(day, maxDays, weekStartPos);
        cout << sWeek << endl;
        weekStartPos=1; //remaining weeks of Month must start on a Monday
    }

}